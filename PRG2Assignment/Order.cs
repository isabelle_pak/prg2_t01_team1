﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Order //Done by: Seng Jun
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; } = new List<Ticket>();
        public Order() { }

        public Order(int orderNo, DateTime orderDateTime)
        {
            OrderNo = orderNo;
            OrderDateTime = orderDateTime;
        }

        public void AddTicket(Ticket ticket)
        {
            TicketList.Add(ticket);
        }
        public override string ToString()
        {
            return "Order No: " + OrderNo + "\tOrder Date Time: " + OrderDateTime + "\tAmount: " + Amount + "\tStatus: " + Status + "\tTicket List: " + TicketList;
        }
    }
}
