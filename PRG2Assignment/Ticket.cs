﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    abstract internal class Ticket //Done by: Isabelle
    {
        public Screening Screening { get; set; }
        public Ticket() { }
        public Ticket(Screening screening) 
        { 
            Screening = screening; 
        }
        public abstract double CalculatePrice();
        public override string ToString()
        {
            return "Screening: " + Screening;
        }
    }
}
