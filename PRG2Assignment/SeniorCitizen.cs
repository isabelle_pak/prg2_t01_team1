﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    internal class SeniorCitizen:Ticket
    {
        public int YearOfBirth { get; set; }
        public SeniorCitizen() { }
        public SeniorCitizen(Screening screening, int yearOfBirth):base(screening)
        {
            YearOfBirth = yearOfBirth;
        }
        public override double CalculatePrice()
        {
            DateTime openingDay = Screening.Movie.OpeningDate;
            DateTime screeningDay = Screening.ScreeningDateTime;
            int dateDiff = (screeningDay.Date - openingDay.Date).Days;

            List<DayOfWeek> weekend = new List<DayOfWeek>() { DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };

            if (Screening.ScreeningType == "3D")
            {
                if (dateDiff < 7)
                {
                    if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                    {
                        return 14.0;
                    }
                    else
                    {
                        return 11.0;
                    }
                }
                else
                {
                    if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                    {
                        return 14.0;
                    }
                    else
                    {
                        return 6.0;
                    }
                }
            }
            else
            {
                if (dateDiff < 7)
                {
                    if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                    {
                        return 12.5;
                    }
                    else
                    {
                        return 8.50;
                    }
                }
                else
                {
                    if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                    {
                        return 12.5;
                    }
                    else
                    {
                        return 5.0;
                    }
                }

            }
        }
        public override string ToString()
        {
            return base.ToString() + "\tYear of Birth: " + YearOfBirth;
        }
    }
}
