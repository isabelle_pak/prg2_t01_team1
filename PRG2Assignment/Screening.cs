﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    internal class Screening : IComparable<Screening> //Done by: Isabelle 
    {
        private int seatsRemaining;

        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }
        public Screening() { }
        public Screening(int screeningNo, DateTime screeningDateTime, string screeningType, Cinema cinema, Movie movie)
        {
            ScreeningNo = screeningNo;
            ScreeningDateTime = screeningDateTime;
            ScreeningType = screeningType;
            Cinema = cinema;
            Movie = movie;
            SeatsRemaining = cinema.Capacity;
        }
        public int CompareTo(Screening tempScreening)
        {
            if (SeatsRemaining > tempScreening.SeatsRemaining)
            {
                return 1;
            }
            else if (SeatsRemaining == tempScreening.SeatsRemaining)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public override string ToString()
        {
            return "Screening No: " + ScreeningNo + "\tScreening Date Time: " + ScreeningDateTime + "\tScreening Type: " + ScreeningType + "\tSeats Remaining: " + SeatsRemaining + "\tCinema:" + Cinema + "\tMovie: " + Movie;
        }
    }
}
