﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Movie : IComparable<Movie> //Done by: Seng Jun
    {
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; } = new List<string>();
        public List<Screening> ScreeningList { get; set; } = new List<Screening>();
        public Movie() { }
        public Movie(string title, int duration, string classification, DateTime openingDate, List<string> genreList)
        {
            Title = title;
            Duration = duration;
            Classification = classification;
            OpeningDate = openingDate;
            GenreList = genreList;
        }
        public void AddScreening (Screening screening)
        {
            ScreeningList.Add(screening);
        }
        public override string ToString()
        {
            return "Title: " + Title + "\tDuration: " + Duration + "\tClassification: " + "\tOpening Date: " + OpeningDate + "\tGenre List: " + GenreList + "\tScreening List: " + ScreeningList;
        }

        public int CompareTo(Movie movie)
        {
            int movie1 = CalculateTicketSold(ScreeningList);
            int movie2 = CalculateTicketSold(movie.ScreeningList);

            if (movie1 > movie2)
            {
                return -1;
            }
            else if (movie1 == movie2)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private static int CalculateTicketSold(List<Screening> screeningList)
        {
            int count = 0;

            foreach (Screening screening in screeningList)
            {
                int capacity = screening.Cinema.Capacity;
                int seatsRemaining = screening.SeatsRemaining;

                int ticketSold = capacity - seatsRemaining;

                count += ticketSold;
            }

            return count;
        }
    }
}
