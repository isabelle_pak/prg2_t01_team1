﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.IO;

namespace PRG2Assignment
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Empty string to store user's option in variable userOption with data type string
            string userOption = "";

            //Empty list movieList with data type Movie to store movie-related data from Movie.csv
            List<Movie> movieList = new List<Movie>();

            //Empty list cinemaList with data type Cinema to store cinema-related data from Cinema.csv
            List<Cinema> cinemaList = new List<Cinema>();

            //Empty list screeningList with data type Screening to store screening-related data from Screening.csv
            List<Screening> screeningList = new List<Screening>();

            //Empty list orderList with data type Order to store, modify or remove order-related data
            //when a new order is created in option [5] or when an order is removed in option [6]
            List<Order> orderList = new List<Order>();

            //Loads Movie and Cinema data (see below for detailed explanation of function)
            LoadMovieCinemaData(movieList, cinemaList);

            //Loads Screening data (see below for detailed explanation of function)
            LoadScreeningData(screeningList, cinemaList, movieList);

            while (userOption != "0")
            {
                DisplayMenu();

                Console.Write("Enter your option: ");
                userOption = Console.ReadLine();

                if (userOption == "1")
                {
                    ListAllMovies(movieList);
                }
                else if (userOption == "2")
                {
                    ListMovieScreenings(screeningList, movieList);
                }
                else if (userOption == "3")
                {
                    AddMovieScreeningSession(screeningList, movieList, cinemaList);
                }
                else if (userOption == "4")
                {
                    DeleteMovieScreeningSession(screeningList);
                }
                else if (userOption == "5")
                {
                    OrderMovieTickets(screeningList, movieList, orderList);
                }
                else if (userOption == "6")
                {
                    CancelTicketOrder(orderList, screeningList);
                }

                else if (userOption == "7")
                {
                    MovieRecommendation(screeningList, movieList, orderList);
                }
                else if (userOption == "8")
                {
                    AvailableSeatsInDescOrder(screeningList);
                }
                else if (userOption == "9")
                {
                    Top3Movie(screeningList, movieList, orderList);
                }
                else if (userOption == "0")
                {
                    Console.WriteLine("Exiting Movie Ticketing System...");
                }
                else
                {
                    Console.WriteLine("\nPlease input an valid option.\n");
                }
            }

        }
        static void DisplayMenu()
        {
            Console.WriteLine("================ Singa Cineplexes ================");
            Console.WriteLine("============= Movie Ticketing System =============\n");
            //Console.WriteLine("[1] Load Movie and Cinema data");                          //Done by: Isabelle
            //Console.WriteLine("[2] Load Screening data");                                 //Done by: Isabelle & Seng Jun
            Console.WriteLine("[1] List all movies");                                       //Done by: Isabelle
            Console.WriteLine("[2] List movie screenings");                                 //Done by: Isabelle & Seng Jun
            Console.WriteLine("[3] Add a movie screening session");                         //Done by: Isabelle & Seng Jun
            Console.WriteLine("[4] Delete a movie screening session");                      //Done by: Isabelle
            Console.WriteLine("[5] Order movie ticket(s)");                                 //Done by: Isabelle & Seng Jun
            Console.WriteLine("[6] Cancel order of ticket");                                //Done by: Isabelle & Seng Jun
            Console.WriteLine("[7] Movie recommended based on tickets sold");               //Done by: Seng Jun
            Console.WriteLine("[8] Display available seats of screening sessions");         //Done by: Isabelle
            Console.WriteLine("[9] Top 3 movies based on tickets sold");                    //Done by: Seng Jun

            Console.WriteLine("[0] Exit Movie Ticketing System\n");
        }

        //Load Movie and Cinema Data  
        static void LoadMovieCinemaData(List<Movie> movieList, List<Cinema> cinemaList)
        {
            //Reads data in Movie.csv and stores it in a string array readMovieCSV
            string[] readMovieCSV = File.ReadAllLines("Movie.csv");

            //Loops through every line in Movie.CSV
            for (int m = 1; m < readMovieCSV.Length; m++)
            {
                //Splits each line based on deliminator ',' and stores it into an array readLineInMovie
                string[] readLineInMovie = readMovieCSV[m].Split(',');

                //Stores movie title (first element in each line) into variable tempMovieTitle
                string tempMovieTitle = readLineInMovie[0];

                //Converts the movie duration (second element in each line) into an integer and stores it in variable tempMovieDuration
                int tempMovieDuration = Convert.ToInt32(readLineInMovie[1]);

                //Stores movie classification (fourth element in each line) into variable tempMovieClassification
                string tempMovieClassification = readLineInMovie[3];

                //Converts the movie opening date (fifth element in each line) into date time format and stores it in variable tempMovieOpeningDate
                DateTime tempMovieOpeningDate = Convert.ToDateTime(readLineInMovie[4]);

                //Empty list individualGenreList to store each movie's genres
                List<string> individualGenreList = new List<string>();

                //Splits movie genres (third element in each line) based on deliminator '/' and stores it into an array movieGenres
                string[] movieGenres = readLineInMovie[2].Split('/');

                //Loops through each genre in the movieGenres array for every movie in movieList
                foreach (string genre in movieGenres)
                {
                    //Adds each genre in the movieGenres array into list individualGenreList
                    individualGenreList.Add(genre);
                }

                //Adds a new movie to movieList based on parameters outlined in Movie class
                movieList.Add(new Movie(tempMovieTitle, tempMovieDuration, tempMovieClassification, tempMovieOpeningDate, individualGenreList));
            }

            //Reads data in Cinema.csv and stores it in a string array readCinemaCSV
            string[] readCinemaCSV = File.ReadAllLines("Cinema.csv");

            //Loops through every line in Movie.CSV
            for (int c = 1; c < readCinemaCSV.Length; c++)
            {
                //Splits each line based on deliminator ',' and stores it into an array readLineInCinema
                string[] readLineInCinema = readCinemaCSV[c].Split(',');

                //Stores cinema name (first element in each line) into variable tempCinemaName
                string tempCinemaName = readLineInCinema[0];

                //Converts the cinema's hall number (second element in each line) into an integer and stores it in variable tempCinemaHallNo
                int tempCinemaHallNo = Convert.ToInt32(readLineInCinema[1]);

                //Converts the capacity of cinema's hall (third element in each line) into an integer and stores it in variable tempCinemaCapacity
                int tempCinemaCapacity = Convert.ToInt32(readLineInCinema[2]);

                //Adds a new cinema to cinemaList based on parameters outlined in Cinema class
                cinemaList.Add(new Cinema(tempCinemaName, tempCinemaHallNo, tempCinemaCapacity));
            }
        }

        //Load Screening data
        static void LoadScreeningData(List<Screening> screeningList, List<Cinema> cinemaList, List<Movie> movieList)
        {
            //Reads data in Screening.csv and stores it in a string array readScreeningCSV
            string[] readScreeningCSV = File.ReadAllLines("Screening.csv");

            //Screening number in Screening class is a running number starting from 1001. 
            int screeningNo = 1001;

            //Loops through every line in Screening.CSV
            for (int i = 1; i < readScreeningCSV.Length; i++)
            {
                //Splits each line based on deliminator ',' and stores it into an array readLineInScreening
                string[] readLineInScreening = readScreeningCSV[i].Split(',');

                //Converts the screening opening date (first element in each line) into date time format and stores it in variable tempScreeningDateTime
                DateTime tempScreeningDateTime = Convert.ToDateTime(readLineInScreening[0]);

                //Stores the screening type (second element in each line) into variable tempScreeningType
                string tempScreeningType = readLineInScreening[1];

                //Stores the cinema name (third element in each line) into variable tempCinemaName
                string tempCinemaName = readLineInScreening[2];

                //Converts the cinema hall no (fourth element in each line) into an integer and stores it in variable tempCinemaHallNo
                int tempCinemaHallNo = Convert.ToInt32(readLineInScreening[3]);

                //Calls the SearchCinema method to search for a cinema based on cinema name (tempCinemaName) and cinema hall number (tempCinemaHallNo)
                //and stores it in variable with data type Cinema tempCinema 
                Cinema tempCinema = SearchCinema(cinemaList, tempCinemaName, tempCinemaHallNo);

                //Stores the movie title (fifth element in each line) into variable tempMovieTitle
                string tempMovieTitle = readLineInScreening[4];

                //Calls the SearchMovie method to search for a movie based on movie title (tempMovieTitle) and stores it in variable with data type Movie tempMovie 
                Movie tempMovie = SearchMovie(movieList, tempMovieTitle);

                //Adds a new screening to screeningList based on parameters outlined in Screening class
                screeningList.Add(new Screening(screeningNo, tempScreeningDateTime, tempScreeningType, tempCinema, tempMovie));

                //Increments screening number by 1 
                screeningNo++;
            }
        }
        //Searches cinema according to the cinema's name and hall number
        static Cinema SearchCinema(List<Cinema> cinemaList, string tempCinemaName, int tempCinemaHallNo)
        {
            //Loops through each cinema in the cinema list 
            foreach (Cinema cinema in cinemaList)
            {
                //Code to run if the cinema's name and hall number matches both tempCinemaName and tempCinemaHallNo
                if (cinema.Name == tempCinemaName && cinema.HallNo == tempCinemaHallNo)
                {
                    //Returns the cinema object as cinema into the function and breaks out of the function
                    return cinema;
                }
            }

            //Returns null if the cinema does not exist (i.e.: specifications of cinema were not met)
            return null;
        }
        //Searches movie according to the movie's title
        static Movie SearchMovie(List<Movie> movieList, string tempMovieTitle)
        {
            //Loops through each movie in the movie list 
            foreach (Movie movie in movieList)
            {
                //Code to run if the movie's title matches tempMovieTitle
                if (movie.Title == tempMovieTitle)
                {
                    //Returns the movie object as movie into the function and breaks out of the function
                    return movie;
                }
            }

            //Returns null if the movie does not exist (i.e.: specifications of movie did not match does which were already in loaded)
            return null;
        }

        //[1] List all movies
        static void ListAllMovies(List<Movie> movieList)
        {
            //Movie counter for user to easily view the number of movies that are currently being screened
            int movieNo = 1;

            Console.WriteLine();

            //Header for displaying all movies in the list movieList
            Console.WriteLine("{0,-5}{1,-30}{2,-19}{3,-20}{4,-15}{5,-30}", "No ", "Title", "Duration (mins)", "Classification", "Opening Date", "Genre");

            //Loops through each movie in the list movieList
            foreach (Movie movie in movieList)
            {
                //Displays the information of each movie in the movieList based on the properties of the movie specified to be displayed
                Console.WriteLine("{0,-5}{1,-30}{2,-19}{3,-20}{4,-15}{5,-30}", $"[{movieNo}]", movie.Title, movie.Duration, movie.Classification, movie.OpeningDate.ToShortDateString(), string.Join(", ", movie.GenreList));

                //Increments the movie counter by 1 for the data of the next movie to be displayed
                movieNo++;
            }

            //Message for user to know that all movies have been displayed and program is running as per normal
            Console.WriteLine("\nAll movies have been displayed.\n");
        }

        //[2] List movie screenings
        static Movie ListMovieScreenings(List<Screening> screeningList, List<Movie> movieList)
        {
            //Defines the variable movie with data type Movie as null 
            Movie movie = null;

            //Empty list to store a movie-specific screenings in a list to see all the screenings for that movie only
            List<Screening> movieScreeningsList = new List<Screening>();

            //List all movies (see above for detailed explanation of function)
            ListAllMovies(movieList);

            try
            {
                //Prompts user to choose a movie based on the numbering when the data is displayed
                //or [0] if they choose to exit this option and go back into the main menu
                Console.Write("Select a movie (Press [0] to exit): ");

                //Converts the user's input of the selected movie which they choose to view the screenings of
                //into type integer and stores it in variable userSelectedMovie
                int userSelectedMovie = Convert.ToInt32(Console.ReadLine());

                //Code to run if user inputs option [0]
                if (userSelectedMovie == 0)
                {
                    //Displays an "Exiting to main menu..." message to show user that it is going back into main menu
                    Console.WriteLine("Exiting...\n");

                    //Return null causes function to stop running and go back to main menu
                    //and prompt user to choose an option from the main menu again
                    return null;
                }

                //Decrements the user's option by 1 to access the movieList based on the movie's index 
                int selectedMovieIndex = userSelectedMovie - 1;

                //Stores the movie object from movieList based on selectedMovieIndex in variable with data type Movie selectedMovieObject
                Movie selectedMovieObject = movieList[selectedMovieIndex];

                //Assigns variable movie the Movie object stored in selectedMovieObject 
                movie = selectedMovieObject;

                //Loops through every screening available in screeningList
                foreach (Screening screening in screeningList)
                {
                    //Code to run if the user-selected movie title matches the title of the movie in a screening session in the screeningList
                    if (selectedMovieObject.Title == screening.Movie.Title)
                    {
                        //Adds the screening object to the movieScreeningList to store all the screening sessions
                        //with the same title (as specified and defined by user)
                        movieScreeningsList.Add(screening);
                    }
                }

                //Code to run if there is at least 1 screening available in screeningList for the movie selected by the user
                if (movieScreeningsList.Count > 0)
                {
                    //Header for displaying all the screening sessions for movie specified by user
                    Console.WriteLine($"\n=== Currently viewing movie screenings for {selectedMovieObject.Title} ===");
                    Console.WriteLine("\n{0,-15}{1,-30}{2,-20}{3,-20}{4,-15}", "Screening No", "Date Time", "Screening Type", "Cinema Name", "Hall Number");

                    //Loops through each movie-specific screening in movieScreeningList (as per user's input of option)
                    foreach (Screening movieScreening in movieScreeningsList)
                    {
                        //Code to run if the movie's screeningList does not contain the screening session
                        if (!movie.ScreeningList.Contains(movieScreening))
                        {
                            //Adds the movie screening stored in movieScreening into the movie-specific screeningList
                            movie.ScreeningList.Add(movieScreening);
                        }
                        //Displays the information of the movie-specific screening based on the properties of the movie screening specified to be displayed
                        Console.WriteLine("{0,-15}{1,-30}{2,-20}{3,-20}{4,-15}", $"[{movieScreening.ScreeningNo}]", movieScreening.ScreeningDateTime, movieScreening.ScreeningType, movieScreening.Cinema.Name, movieScreening.Cinema.HallNo);
                    }
                    Console.WriteLine();
                }

                //Code to run if there are no screenings available/allocated in screeningList for the movie selected by the user
                else
                {
                    Console.WriteLine($"\nThere are no existing scheduled screenings for {selectedMovieObject.Title}.\n");
                }
            }

            //Catches any error if the user enters an invalid option which is not supported by the program
            catch (Exception exception)
            {
                //Displays the error message which caused the program to break
                Console.WriteLine(exception.Message);

                //Prompts the user to enter a valid option for program to run smoothly
                Console.WriteLine("Please enter a valid option.\n");

                //return null causes function to stop running and go back to main menu
                //and prompt user to choose an option from the main menu again
                return null;
            }

            //Returns movie object to be stored in variable with data type Movie movie in option [5] for OrderMovieTickets() function
            return movie;
        }

        //[3] Add a movie screening session
        static void AddMovieScreeningSession(List<Screening> screeningList, List<Movie> movieList, List<Cinema> cinemaList)
        {
            //List all movies (see above for detailed explanation of function)
            ListAllMovies(movieList);
            
            try
            {
                //Prompts user to choose a movie based on the numbering when the data is displayed
                //or 0 if they choose to exit this option and go back into the main menu
                Console.Write("Select a movie (Press [0] to exit): ");

                //Converts the user's input of the selected movie which they want to add screenings for
                //into type integer and stores it in variable selectedMovieOption
                int selectedMovieOption = Convert.ToInt32(Console.ReadLine());

                //Code to run if user inputs option [0]
                if (selectedMovieOption == 0)
                {
                    //Displays an "Exiting..." message to show user that it is going back into main menu
                    Console.WriteLine("Exiting...\n");

                    //return causes function to stop running and go back to main menu
                    //and prompt user to choose an option from the main menu again
                    return;
                }

                //Decrements selectedMovieOption by 1 to access a movie in the movieList using its index
                //Stores the movie object from movieList based on selectedMovieOption in variable with data type Movie selectedMovie
                Movie selectedMovie = movieList[selectedMovieOption - 1];

                //Empty string to store user's option in variable selectedScreeningType with data type string
                string selectedScreeningType = "";

                //Code to run continuously while the screening type entered is not 2D or 3D
                while (!(selectedScreeningType == "2D" || selectedScreeningType == "3D"))
                {
                    //Prompts user to choose a screening type for the new screening to be added
                    Console.Write("\nEnter a screening type [2D/3D]: ");

                    //Reads the user's input and converts it to uppercase and stores it in variable selectedScreeningType
                    selectedScreeningType = Console.ReadLine().ToUpper();

                    //Code runs if the user inputs option [0]
                    if (selectedScreeningType == "0")
                    {
                        //AddMovieScreeningSession function is called again if the user inputs option [0] to exit current page
                        //and goes back to the ask the user to select a movie to add a screening session to again
                        AddMovieScreeningSession(screeningList, movieList, cinemaList);

                        //Return prevents code outside this if-else statement from running and the function to stop running
                        //and go back to main menu to prompt user to choose an option from the main menu again
                        return;
                    }

                    //Codes runs if the user inputs any option other than "2D" or "3D"
                    else if (selectedScreeningType != "2D" && selectedScreeningType != "3D")
                    {
                        //Prompts user to enter a valid option ([2D] or [3D]) or enter 0 to exit
                        Console.Write("Please select 2D or 3D screening type (Enter [0] to exit)\n");
                    }
                }

                //Empty list movieScreeningList with data type Screening to store the movie-specific screening-related data
                List<Screening> movieScreeningList = new List<Screening>();

                //Loops through every screening in the orginal screeningList loaded in LoadScreeningData function
                foreach (Screening screening in screeningList)
                {
                    //Code to run if the user-selected movie title matches the title of the movie in a screening session in the screeningList
                    if (screening.Movie.Title == selectedMovie.Title)
                    {
                        //Adds the screening object to the movieScreeningList to store all the screening sessions
                        //with the same title (as specified and indicated by user and stored in variable selectedMovieOption)
                        movieScreeningList.Add(screening);
                    }
                }

                //Stores a temporary value to variable with DateTime value selectedScreeningDateTime
                DateTime selectedScreeningDateTime = DateTime.MinValue;

                //Code to run continuously while the DateTime value entered is invalid 
                while (selectedScreeningDateTime == DateTime.MinValue)
                {
                    //Prompts user to enter a screening date and time for the movie to be screened based on the given format as shown
                    Console.Write("\nEnter a screening date and time (eg: 12/08/2022 15:00): ");

                    //Converts the date and time entered by the user of the new screening to be added and stores it in variable
                    //with data type DateTime enteredScreeningDateTime
                    DateTime enteredScreeningDateTime = Convert.ToDateTime(Console.ReadLine());

                    //Checks if DateTime value stored in enteredScreeningDateTime is after the current date time and
                    //if it is after the selected movie's opening date and stores the boolean value in variable isAfterTodayAndOpeningDate
                    bool isAfterTodayAndOpeningDate = enteredScreeningDateTime > DateTime.Now && enteredScreeningDateTime > selectedMovie.OpeningDate;

                    //Code runs if the variable isAfterTodayAndOpeningDate returns true
                    if (isAfterTodayAndOpeningDate)
                    {
                        //Validates that the date time entered by the user is valid
                        Console.WriteLine("Valid date time entered.");
                    }

                    //Code runs if the variable isAfterTodayAndOpeningDate returns false
                    else
                    {
                        //Prompts user that invalid date time has been entered by the user
                        Console.WriteLine("Invalid date time entered: cannot create screening before today.\n");

                        //Automatically returns back to the start of the AddMovieScreeningSession function for the user to change the start
                        //date time of a movie screening session since a valid date time is required before a screening can be created
                        AddMovieScreeningSession(screeningList, movieList, cinemaList);

                        //Return prevents code outside this if-else statement from running and the function to stop running
                        //and go back to main menu to prompt user to choose an option from the main menu again
                        return;
                    }

                    //Header for displaying all the screening sessions for movie specified by user
                    Console.WriteLine("\n{0,-5}{1,-18}{2,-14}{3,-11}", "No", "Cinema Name", "Hall Number", "Capacity");
                    
                    //Loops through each element in list cinemaList 
                    for (int i = 0; i < cinemaList.Count; i++)
                    {
                        //Retreives the cinema object from cinemaList based on its index i and stores it in a variable with data type Cinema cinema
                        Cinema cinema = cinemaList[i];

                        //Displays the information of the cinema-related data based on the properties of the cinema specified to be displayed
                        Console.WriteLine("{0,-5}{1,-18}{2,-14}{3,-11}", $"[{i + 1}]", cinema.Name, cinema.HallNo, cinema.Capacity);
                    }

                    //Prompts the user to enter a cinema which they wish to screen the movie in
                    Console.Write("\nSelect a cinema: ");

                    //Converts the number of the cinema based on the table displayed into an integer and stores it in variable cinemaIndex
                    int cinemaOption = Convert.ToInt32(Console.ReadLine());

                    //Decrements cinemaOption by 1 to access a cinema in the movieList using its index
                    //Stores the cinema object from cinemaList based on cinemaOption in variable with data type Cinema selectedCinema
                    Cinema selectedCinema = cinemaList[cinemaOption - 1];

                    //Assigns a default boolean value of true to the variable screeningSessionAvailable 
                    bool screeningSessionAvailable = true;

                    //Loops through every screening in the the screeningList
                    foreach (Screening screening in screeningList)
                    {
                        //Stores the cleaning duration required after each screening session into variable with data type integer cleaningDuration
                        int cleaningDuration = 30;

                        //Stores each movie's screening session start date and time to variable with data type DateTime movieStartDateTime 
                        DateTime movieStartDateTime = screening.ScreeningDateTime;

                        //Adds the movie's duration and the time required to clean up the hall (cleaningDuration) to its start time
                        //Stores each movie's screening session end date and time to variable with data type DateTime movieEndDateTime 
                        DateTime movieEndDateTime = screening.ScreeningDateTime.AddMinutes(screening.Movie.Duration)
                                                                          .AddMinutes(cleaningDuration);

                        //Code runs if the entered screening time clashes with any of the screening start or end date times
                        //and if the cinema name and hall number matches a screening session which is already in screeningList
                        if (movieStartDateTime <= enteredScreeningDateTime
                            && enteredScreeningDateTime <= movieEndDateTime
                            && screening.Cinema.Name == selectedCinema.Name
                            && screening.Cinema.HallNo == selectedCinema.HallNo)
                        {
                            //Assigns a boolean value of false to the variable screeningSessionAvailable => screening session is not available
                            screeningSessionAvailable = false;

                            //Breaks out of the for-loop
                            break;
                        }
                    }

                    //Code to run if screeningSessionAvailable still returns true after going through the for-loop above
                    if (screeningSessionAvailable == true)
                    {
                        //Stores the DateTime value stored in enteredScreeningDateTime in the selectedScreeningDateTime 
                        selectedScreeningDateTime = enteredScreeningDateTime;

                        //Stores the screening number of the movie screening session to be added by counting the number of screenings
                        //in the list and adding 1001 to get the newScreeningNo
                        int newScreeningNo = screeningList.Count + 1001;

                        //Adds the new screening to screeningList based on parameters outlined in Screening class
                        screeningList.Add(new Screening(newScreeningNo, selectedScreeningDateTime, selectedScreeningType, selectedCinema, selectedMovie));

                        //Message for user to know that the movie screening which they want to add has been added successfully
                        Console.WriteLine($"\nThe movie screening for {selectedMovie.Title} on {selectedScreeningDateTime} has been successfully created.\n");
                    }

                    //Code to run if screeningSessionAvailable returns false => movie screening session was not able to be created successfully 
                    else
                    {
                        //Message for user to know that the movie screening which they want to add has been added successfully
                        Console.WriteLine($"The movie screening for {selectedMovie.Title} has not been created successfully. Please try again.");
                    }
                }
            }

            //Catches any error if the user enters an invalid option which is not supported by the program
            catch (Exception exception)
            {
                //Displays the error message which caused the program to break
                Console.WriteLine(exception.Message);

                //Displays the error message which caused the program to break
                Console.WriteLine("Please enter a valid option.\n");

                //Automatically returns back to the start of the AddMovieScreeningSession for the user 
                //to attempt creating the new movie screening session again
                AddMovieScreeningSession(screeningList, movieList, cinemaList);
            }
        }

        //[4] Delete a movie screening session
        static void DeleteMovieScreeningSession(List<Screening> screeningList)
        {
            //Empty list screeningList with data type Screening to store movie screenings which have not sold any seats
            List<Screening> unpopularMovieScreenings = new List<Screening>();

            //Loops through every screening session in screeningList 
            foreach (Screening screeningSession in screeningList)
            {
                //Code to run if the number of seats remaining in a screening session is equal to the cinema's original capacity => no tickets have been sold
                if (screeningSession.SeatsRemaining == screeningSession.Cinema.Capacity)
                {
                    //Add the screening object to list unpopularMovieScreenings 
                    unpopularMovieScreenings.Add(screeningSession);
                }
            }

            //Header for displaying movie screening sessions which have not sold any tickets in the list unpopularMovieScreenings
            Console.WriteLine("=== Movie screening sessions which have not sold any tickets ===");
            Console.WriteLine("\n{0,-15}{1,-23}{2,-30}{3,-20}{4,-20}{5,-15}", "Screening No", "Movie Title", "Date Time", "Screening Type", "Cinema Name", "Hall Number");

            //Loops through every screening which did not sell any seats stored in list unpopularMovieScreenings
            foreach (Screening unpopularScreening in unpopularMovieScreenings)
            {
                //Displays the information of each screening in the unpopularScreeningList based on the properties of the screening specified to be displayed
                Console.WriteLine("{0,-15}{1,-23}{2,-30}{3,-20}{4,-20}{5,-15}", $"[{unpopularScreening.ScreeningNo}]", unpopularScreening.Movie.Title, unpopularScreening.ScreeningDateTime, unpopularScreening.ScreeningType, unpopularScreening.Cinema.Name, unpopularScreening.Cinema.HallNo);
            }

            try
            {
                //Prompts user to choose a movie screening to be removed based on the screening no
                //or choose [0] to exit this option and go back into the main menu
                Console.Write("\nSelect a movie screening to be removed (press [0] to exit): ");

                //Converts the user's input of the selected screening which they choose to remove 
                //into type integer and stores it in variable screeningNoToRemove
                int screeningNoToRemove = Convert.ToInt32(Console.ReadLine());

                //Code to run if the user inputs option [0]
                if (screeningNoToRemove == 0)
                {
                    //Displays an "Exiting to main menu..." message to show user that it is going back into main menu
                    Console.WriteLine("Exiting to main menu...\n");

                    //Return prevents code outside this if-else statement from running and the function to stop running
                    //and go back to main menu to prompt user to choose an option from the main menu again
                    return;
                }

                //Code to run as long the user enters a valid screening number to be removed
                else
                {
                    //Loops through each element in list unpopularMovieScreenings  
                    for (int s = 0; s < unpopularMovieScreenings.Count; s++)
                    {
                        //Code to run if the screening number of the screening to be removed is found
                        //inside the unpopularMovieScreenings list
                        if (screeningNoToRemove == unpopularMovieScreenings[s].ScreeningNo)
                        {
                            //Removes the specified screening from the screeningList based on its screening number
                            screeningList.Remove(screeningList[s]);
                            
                            //Message for user to know that the movie screening which they selected to be removed
                            //has been successfully removed from list screeningList
                            Console.WriteLine("This movie screening has been successfully removed.\n");
                            
                            //Breaks out of the for-loop
                            break;
                        }

                        //Code to run if code successfully runs till the last screening element in list unpopularMovieScreenings and the screening number
                        //of the screening to be removed is not equal to the screening number of the final screening in list unpopularMovieScreenings
                        else if (s == unpopularMovieScreenings.Count - 1 && !(screeningNoToRemove == unpopularMovieScreenings[s].ScreeningNo))
                        {
                            //Message for user to know that the movie screening which they want to remove was not successful
                            Console.WriteLine("This movie screening has not been removed successfully. Please try again.\n");
                        }
                    }
                }
            }

            //Catches any error if the user enters an invalid option which is not supported by the program
            catch (Exception exception)
            {
                //Displays the error message which caused the program to break
                Console.WriteLine(exception.Message);

                //Prompts the user to enter a valid option for program to run smoothly
                Console.WriteLine("Please enter a valid option.\n");

                //Automatically returns back to the start of the DeleteMovieScreeningSession function for the user 
                //to attempt deleting the new movie screening session again
                DeleteMovieScreeningSession(screeningList); 
            }
        }

        //[5] Order movie ticket(s)
        static void OrderMovieTickets(List<Screening> screeningList, List<Movie> movieList, List<Order> orderList)
        {
            //List movie screening of movie selected by user (see above for detailed explanation of function)
            //and stores it in variable with data type Movie movieScreening
            Movie movieScreening = ListMovieScreenings(screeningList, movieList);

            try
            {
                //Code to run if the movieScreening returns null => there are no movie screenings for the chosen movie
                if (movieScreening == null)
                {
                    //Return prevents code outside this if-else statement from running and the function to stop running
                    //and go back to main menu to prompt user to choose an option from the main menu again
                    return;
                }

                //Code to run if a movieScreening is not contained inside list screeningList
                if (!ContainsScreeningInList(movieScreening, screeningList))
                {
                    //Automatically returns back to the start of the OrderMovieTickets for the user to attempt ordering movie tickets again
                    OrderMovieTickets(screeningList, movieList, orderList);

                    //Return prevents code outside this if-else statement from running and the function to stop running
                    //and go back to main menu to prompt user to choose an option from the main menu again
                    return;
                }

                //Prompts the user to enter a movie screening number to order tickets for
                Console.Write("Select a movie screening: ");

                //Converts the user's input of the selected movie screening which they choose to order tickets for
                //into type integer and stores it in variable userSelectedMovieScreeningNo
                int userSelectedMovieScreeningNo = Convert.ToInt32(Console.ReadLine());

                //Searches for the screening according to the movie screening number input by the user (userSelectedMovieScreeningNo)
                Screening retrievedMovieScreening = SearchScreening(screeningList, userSelectedMovieScreeningNo);

                //Code to run if an invalid movie screening number was entered or if the movie title's of movieScreening and retrievedMovieScreening do not match
                if ((retrievedMovieScreening == null) || (!ScreeningMovieTitleMatchesMovieTitle(retrievedMovieScreening, movieScreening)))
                {
                    //Message to tell user that an invalid movie screening option has been entered
                    Console.WriteLine("There is no such movie screening.\n");

                    //Automatically returns back to the start of the OrderMovieTickets function for the user
                    //to attempt ordering movie tickets again with valid data inputs
                    OrderMovieTickets(screeningList, movieList, orderList);

                    //Return prevents code outside this if-else statement from running and the function to stop running
                    //and go back to main menu to prompt user to choose an option from the main menu again
                    return;
                }

                //Variable storing the number of tickets the user intends to order
                int userNoOfTickets = 0;

                //Code to run continuously while true
                while (true)
                {
                    //Prompts user to enter the number of tickets they would like to order
                    Console.Write("\nEnter the total number of tickets to order: ");

                    //Converts the user's input of the number of tickets they would like to order
                    //into type integer and stores it in variable userNoOfTix
                    int userNoOfTix = Convert.ToInt32(Console.ReadLine());
                    
                    //Code to run if the number of tickets the user would like to order is greater
                    //than the number of seats available for the screening session selected
                    if (userNoOfTix > retrievedMovieScreening.SeatsRemaining)
                    {
                        //Message to inform users that there are insufficient seats for that movie screening to fulfill their order
                        Console.WriteLine("\nThere are insufficient available seats to fulfill your order.");
                        
                        //Goes back to start of the while-loop to ask user to enter the number of tickets they would like to order again
                        continue;
                    }

                    //Code to run if the user enters a valid number of tickets which is less than
                    //or equal to the number of seats remaining for that screening
                    else
                    {
                        //Assigns the number of tickets stored in variable userNoOfTix to userNoOfTickets
                        userNoOfTickets = userNoOfTix;
                    }

                    //Breaks out of the while-loop and continues in the main OrderMovieTickets function
                    break;
                }

                //Code to run when the movie classification of the screening object stored in retrievedMovieScreening is not [G]
                if (retrievedMovieScreening.Movie.Classification != "G")
                {
                    //Message to inform the user that the movie requires age verification of the ticket holders
                    Console.WriteLine("This movie requires age verification.");
                    
                    //Variable storing the minTicketHolderAge for a movie with classification of [G]
                    int minTicketHolderAge = 0;

                    //Code to run if the movie classification of the screening object stored in retrievedMovieScreening is [PG13]
                    if (retrievedMovieScreening.Movie.Classification == "PG13")
                    {
                        //Stores the minimum age to be able to watch PG13 movie => must be at least 13 years old
                        minTicketHolderAge = 13;
                       
                        //Stores boolean value returned from CheckTicketHolderAge function inside variable canWatchMovie
                        bool canWatchMovie = CheckTicketHolderAge(userNoOfTickets, minTicketHolderAge);

                        //Code to run if a ticket holder in an order is underaged (below 13 years old) and is unable to watch the age-restricted movie
                        if (!canWatchMovie)
                        {
                            //Automatically returns back to the start of the OrderMovieTickets function for the user
                            //to choose another movie to order tickets for since a member of the party is underaged
                            OrderMovieTickets(screeningList, movieList, orderList);

                            //Return prevents code outside this if-else statement from running and the function to stop running
                            //and go back to main menu to prompt user to choose an option from the main menu again
                            return;
                        }
                    }

                    //Code to run if the movie classification of the screening object stored in retrievedMovieScreening is [NC16]
                    else if (retrievedMovieScreening.Movie.Classification == "NC16")
                    {
                        //Stores the minimum age to be able to watch NC16 movie => must be at least 16 years old
                        minTicketHolderAge = 16;

                        //Stores boolean value returned from CheckTicketHolderAge function inside variable canWatchMovie
                        bool canWatchMovie = CheckTicketHolderAge(userNoOfTickets, minTicketHolderAge);

                        //Code to run if a ticket holder in an order is underaged (below 16 years old) and is unable to watch the age-restricted movie
                        if (!canWatchMovie)
                        {
                            //Automatically returns back to the start of the OrderMovieTickets function for the user
                            //to choose another movie to order tickets for since a member of the party is underaged
                            OrderMovieTickets(screeningList, movieList, orderList);

                            //Return prevents code outside this if-else statement from running and the function to stop running
                            //and go back to main menu to prompt user to choose an option from the main menu again
                            return;
                        }
                    }

                    //Code to run if the movie classification of the screening object stored in retrievedMovieScreening is [M18]
                    else if (retrievedMovieScreening.Movie.Classification == "M18")
                    {
                        //Stores the minimum age to be able to watch M18 movie => must be at least 18 years old
                        minTicketHolderAge = 18;

                        //Stores boolean value returned from CheckTicketHolderAge function inside variable canWatchMovie
                        bool canWatchMovie = CheckTicketHolderAge(userNoOfTickets, minTicketHolderAge);

                        //Code to run if a ticket holder in an order is underaged (below 18 years old) and is unable to watch the age-restricted movie
                        if (!canWatchMovie)
                        {
                            //Automatically returns back to the start of the OrderMovieTickets function for the user
                            //to choose another movie to order tickets for since a member of the party is underaged
                            OrderMovieTickets(screeningList, movieList, orderList);

                            //Return prevents code outside this if-else statement from running and the function to stop running
                            //and go back to main menu to prompt user to choose an option from the main menu again
                            return;
                        }
                    }

                    //Code to run if the movie classification of the screening object stored in retrievedMovieScreening is [R21]
                    else if (retrievedMovieScreening.Movie.Classification == "R21")
                    {
                        //Stores the minimum age to be able to watch R21 movie => must be at least 21 years old
                        minTicketHolderAge = 21;

                        //Stores boolean value returned from CheckTicketHolderAge function inside variable canWatchMovie
                        bool canWatchMovie = CheckTicketHolderAge(userNoOfTickets, minTicketHolderAge);

                        //Code to run if a ticket holder in an order is underaged (below 21 years old) and is unable to watch the age-restricted movie
                        if (!canWatchMovie)
                        {
                            //Automatically returns back to the start of the OrderMovieTickets function for the user
                            //to choose another movie to order tickets for since a member of the party is underaged
                            OrderMovieTickets(screeningList, movieList, orderList);

                            //Return prevents code outside this if-else statement from running and the function to stop running
                            //and go back to main menu to prompt user to choose an option from the main menu again
                            return;
                        }
                    }
                }

                //Code to run when the movie classification of the screening object stored in retrievedMovieScreening is [G]
                else
                {
                    //Message to tell user that the movie chosen to order tickets for does not require age verification
                    Console.WriteLine("This movie does not require age verification.\n");
                }

                //Calls CreateOrderStatus function to create a new order in the orderList and stores the new order created in variable with data type Order currentOrder 
                Order currentOrder = CreateOrderStatus(orderList);
                
                //Displays a message for the user to know that their order has been created but has yet to be paid.
                Console.WriteLine("An order with status UNPAID has been created.\n");

                //Loops through and continuously asks the user for the type of ticket they would like to order for the number of tickets which they indicated they are ordering 
                for (int i = 1; i <= userNoOfTickets; i++)
                {
                    //Prompts user to input the ticket type for each ticket in the current order (see below for detailed explanation of function)
                    TicketChecker(retrievedMovieScreening, currentOrder);
                }

                //Calculates and displays the ticket price breakdown together with the total payable 
                //Prompts user to either confirm or cancel their order (see below for detailed explanation of function)
                CalculateTicketPrice(orderList, currentOrder);
            }

            //Catches any error if the user enters an invalid option which is not supported by the program
            catch (Exception exception)
            {
                //Displays the error message which caused the program to break
                Console.WriteLine(exception.Message);

                //Prompts the user to enter a valid option for program to run smoothly
                Console.WriteLine("Please enter a valid option.\n");

                //Automatically returns back to the start of the DeleteMovieScreeningSession for the user 
                //to attempt deleting the new movie screening session again
                OrderMovieTickets(screeningList, movieList, orderList);
            }
        }

        //Checks if a screening's movie title in list screeningList matches the movie's title
        static bool ContainsScreeningInList(Movie movie, List<Screening> screeningList)
        {
            //Loops through every screening in list screeningList 
            foreach (Screening screening in screeningList)
            {
                //Code to run if the screening's movie title matches the movie title of the movie entered by the user
                if (screening.Movie.Title == movie.Title)
                {
                    //Returns boolean value true and stores it in the function
                    return true;
                }
            }

            //Returns boolean value false and stores it in the function => screening does not exist in screeningList
            return false;
        }

        //Checks if the movie title of the screening session matches the movie title
        static bool ScreeningMovieTitleMatchesMovieTitle(Screening screening, Movie movie)
        {
            //Code to run if the screening's movie title matches the movie's title
            if (screening.Movie.Title == movie.Title)
            {
                //Returns boolean value true and stores it in the function
                return true;
            }

            //Returns boolean value false and stores it in the function => screening does not exist in screeningList
            return false;
        }

        //Searches for the screening according to the movie screening number input by the user
        static Screening SearchScreening(List<Screening> screeningList, int userSelectedMovieScreeningNo)
        {
            //Loops through every screening stored in list screeningList
            foreach (Screening screening in screeningList)
            {
                //Code to run if the screening's screening number matches the screening number entered by the user
                if (screening.ScreeningNo == userSelectedMovieScreeningNo)
                {
                    //Returns the Screening object into the function SearchScreening function
                    return screening;
                }
            }
            //Returns null value and stores it in the function => screening does not exist in screeningList
            return null;
        }

        //Checks whether the user is able to watch an age-restricted movie based on the minimum age as per the minimum age allowed for the movie classification
        static bool CheckTicketHolderAge(int userNoOfTickets, int minTicketHolderAge)
        {
            //Default boolean value true assigned to variable ageIsValid
            bool ageIsValid = true;

            //Loops through and continuously asks the user for the age of the ticket holders for the number of tickets which the user wants to order
            for (int i = 1; i <= userNoOfTickets; i++)
            {
                //Prompts user to enter the enter the age of each of the ticket holder
                Console.Write($"\nEnter age of ticket holder {i}: ");

                //Converts the user's input of the ticket holder's age into type integer and stores it in variable tempTicketHolderAge
                int tempTicketHolderAge = Convert.ToInt32(Console.ReadLine());
                
                //Code to run if the age of a ticket holder input by the user is less than the minimum age required as stored in variable minTicketHolderAge
                if (tempTicketHolderAge < minTicketHolderAge)
                {
                    //Changes boolean value assigned to ageIsValid to false
                    ageIsValid = false;
                    
                    //Displays an error message informing the user that the ticket holder is underaged and is unable to watch the movie
                    Console.WriteLine("Age Restricted: This ticket holder is underaged and cannot watch this movie.\n");
                    
                    //Breaks out of the for-loop to go back into the OrderMovieTickets function and returns a boolean value false stored in ageIsValid
                    break;
                }

                //Code to run if the ticket holder's age meets the minimum requirements as outlined in variable minTicketHolderAge
                else
                {
                    //Message to inform users that the ticket holder has met the age requirements and is able to watch the movie
                    Console.WriteLine("Age Verfication Success!!");
                }
            }
            Console.WriteLine();
            
            //Returns the boolean value stored in the variable ageIsValid to be stored in the function and assigned to variable canWatchMovie
            return ageIsValid;
        }

        //Creates a new order in the order list and returns it into the variable currentOrder
        static Order CreateOrderStatus(List<Order> orderList)
        {
            //Stores the order number of the movie screening session to be added by counting the number of orders in list orderList and adding 1 to get the orderNo
            int orderNo = orderList.Count + 1;
            
            //Stores the current date time (date time stamp when the order is made)
            DateTime orderDateTime = DateTime.Now;

            //Creates a new order based on parameters outlined in Order class and stores it in variable with data type Order tempOrder
            Order tempOrder = new Order(orderNo, orderDateTime);
            
            //Updates the status of the new order created to be [UNPAID]
            tempOrder.Status = "Unpaid";
            
            //Adds the new order stored in variable tempOrder to list orderList
            orderList.Add(tempOrder);
            
            //Returns the order back into the function and is saved in variable with data type Order currentOrder
            return tempOrder;
        }

        //Prompts user to input the ticket type for each ticket in the current order, currentOrder, as per each of the ticket types available
        static void TicketChecker(Screening retrievedMovieScreening, Order currentOrder)
        {
            //Variable storing a default value for the ticket type to be entered by the user
            int userTicketType = 0;

            //Listing of all the avaiable ticket types for the ticket holder to come under
            Console.WriteLine("=== Available Ticket Types ===");
            Console.WriteLine("[1] Student");
            Console.WriteLine("[2] Senior Citizen");
            Console.WriteLine("[3] Adult");
            
            //Any other consumers who do not come under [Student] or [Senior Citizen] categories will automatically come under the Adult ticket type
            Console.WriteLine("Didn't see a suitable ticket type? Pick [3]");

            try
            {
                //Code to run continuously while the user does not enter a valid choice [1, 2 or 3] of ticket type for each of the ticket holders in the party
                while (!(0 < userTicketType && userTicketType < 4))
                {
                    //Prompts the user to enter the ticket type for a ticket holder in the party
                    Console.Write("\nEnter desired ticket type: ");

                    //Converts the user's input of the selected ticket type which they choose for each ticket 
                    //into type integer and stores it in variable userTicketType
                    userTicketType = Convert.ToInt32(Console.ReadLine());
                }

                //Code to run if user enters option [1] 
                if (userTicketType == 1)
                {
                    //Stores a list of the levels of study for students in list studyLevels
                    List<string> studyLevels = new List<string>() { "Primary", "Secondary", "Tertiary" };

                    //Header for displaying all movies in the list movieList
                    Console.WriteLine("=== Level of Study ===");
                    
                    //Loops through each element in the list studyLevels
                    for (int l = 0; l < studyLevels.Count; l++)
                    {
                        //Displays the levels of study with the option number beside it
                        Console.WriteLine($"[{l + 1}] {studyLevels[l]}");
                    }

                    //Variable storing a default value for the ticket holder's level of study to be entered by the user
                    int userLevelOfStudy = 0;

                    //Code to run continuously while the user does not enter a valid choice [1, 2 or 3] of the level of study for the ticket holder 
                    while (!(1 <= userLevelOfStudy && userLevelOfStudy <= 3))
                    {
                        //Prompts user to enter the level of study for the student
                        Console.Write("Enter level of study: ");    

                        //Converts the option entered by the user into an integer and stores it in variable userLevelOfStudy
                        userLevelOfStudy = Convert.ToInt32(Console.ReadLine());

                        //Code to run if the user option entered is any number other than the valid options [1, 2 or 3]
                        if (!(1 <= userLevelOfStudy && userLevelOfStudy <= 3))
                        {
                            //Displays a message to prompt the user that an invalid option has been entered and asks for the user to enter a valid option again
                            Console.WriteLine("Invalid level of study entered. Please enter a valid option.\n");
                        }
                    }

                    //Code to run if the user enters a valid option [1, 2 or 3] for the level of study of the student
                    if (userLevelOfStudy == 1 || userLevelOfStudy == 2 || userLevelOfStudy == 3)
                    {
                        //Displays a message to show the user that the student discount has been activated for the ticket
                        Console.WriteLine("Student discount activated.\n");

                        //Creates a new student obejct based on parameters outlined in Student class and stores it in variable with data type Student studentTicket
                        Student studentTicket = new Student(retrievedMovieScreening, studyLevels[userLevelOfStudy - 1]);
                        
                        //Adds the student ticket object into currentOrder's ticket list 
                        currentOrder.TicketList.Add(studentTicket);
                        
                        //Decrements the number of seats remaining for the chosen movie screening by 1 once a ticket has been ordered
                        retrievedMovieScreening.SeatsRemaining--;
                    }
                }

                //Code to run if user enters option [2]
                else if (userTicketType == 2)
                {
                    //Empty variable to store year of birth entered by the user
                    int userYear;

                    //Empty variable to store the user's age, derived by subtracting the year of birth from the current year
                    int userAge;

                    //Code to run continuously while the user does not enter a valid year of birth to qualify for the senior citizen discount
                    while (true)
                    {
                        //Prompts the user to enter the year of birth of the ticket holder 
                        Console.Write("Enter year of birth: ");

                        //Converts the user's input of the year of birth of the ticket holder into type integer and stores it variable userYear
                        userYear = Convert.ToInt32(Console.ReadLine());

                        //Converts the current year into type integer and subtracts userYear from userYear
                        //to derive the user's age and is stored in varaible with data type integer userAge
                        userAge = Convert.ToInt32(DateTime.Now.Year) - userYear;

                        //Code to run if the user enters a year of birth which causes the value stored in userAge to be negative or greater than 100
                        if (!(userAge > 0 && userAge < 100))
                        {
                            //Message to inform the user that the year of birth entered is invalid 
                            Console.WriteLine($"Your age is {userAge}. This age is not acceptable. Please enter a valid year of birth.\n");
                        }
                        
                        //Code to run if the user enters a year of birth which results in a valid age being stored in variable userAge
                        else
                        {
                            //Breaks out of the while-loop to continue to code below this while-loop block
                            break;
                        }
                    }

                    //Code to run if the user enters an age which is eligible to receive the senior citizen discount
                    if (userAge >= 55)
                    {
                        //Displays a message to show the user that the senior citizen discount has been activated for the ticket
                        Console.WriteLine("Senior citizen discount activated.\n");

                        //Creates a new SeniorCitizen obejct based on parameters outlined in SeniorCitizen class and stores it in variable with data type SeniorCitizen seniorCitizenTicket
                        SeniorCitizen seniorCitizenTicket = new SeniorCitizen(retrievedMovieScreening, userYear);

                        //Adds the SeniorCitizen ticket object into currentOrder's ticket list 
                        currentOrder.TicketList.Add(seniorCitizenTicket);

                        //Decrements the number of seats remaining for the chosen movie screening by 1 once a ticket has been ordered
                        retrievedMovieScreening.SeatsRemaining--;
                    }

                    //Code to run if the user enters an age which is less than 55 years old
                    else
                    {
                        //Displays a message to show the user that the ticket holder is ineligible to receive senior citizen discount
                        Console.WriteLine("You are not eligible to receive senior citizen discount.\n");
                        
                        //Goes back to the start of the TicketChecker function and prompts user to enter the ticket types for each of the tickets in the currentOrder again
                        TicketChecker(retrievedMovieScreening, currentOrder);

                        //Return prevents code outside this if-else statement from running and the function to stop running
                        //and go back to main menu to prompt user to choose an option from the main menu again
                        return;
                    }
                }

                //Code to run if user enters option [3] 
                else if (userTicketType == 3)
                {
                    //Asks user if the adult ticket holder would like to add a popcorn to their order for $3
                    Console.Write("Popcorn for $3 [Y/N]: ");

                    //Emply boolean variable to be assigned true or false depending on whether the user chooses to add popcorn to their ticket
                    bool userWantsPopcorn;

                    //Code to run continuously while the user does not enter a valid option [Y or N] 
                    while (true)
                    {
                        //Reads the user's input and converts it to all uppercase before storing it in variable userOptionPopcorn
                        string userOptionPopcorn = Console.ReadLine().ToUpper();

                        //Code to run if the user enters option [Y] 
                        if (userOptionPopcorn == "Y")
                        {
                            //Assigns boolean value true to the variable userWantsPopcorn => user adds $3 for ticket and popcorn
                            userWantsPopcorn = true;
                            
                            //Breaks out of while-loop to continue to code below this while-loop
                            break;
                        }

                        //Code to run if the user enters option [N] 
                        else if (userOptionPopcorn == "N")
                        {
                            //Assigns boolean value false to the variable userWantsPopcorn => user wants only ticket
                            userWantsPopcorn = false;

                            //Breaks out of while-loop to continue to code below this while-loop
                            break;
                        }
                        
                        //Message to be displayed if the user does not input a valid option (input options beside [Y or N])
                        Console.WriteLine("Please select [Y] or [N] only.");
                        
                        //Prompts user to enter a valid option again as to whether they would like to add popcorn to their ticket or not
                        Console.Write("Popcorn for $3 [Y/N]: ");
                    }

                    //Creates a new Adult obejct based on parameters outlined in Adult class and stores it in variable with data type Adult adultTicket
                    Adult adultTicket = new Adult(retrievedMovieScreening, userWantsPopcorn);

                    //Adds the SeniorCitizen ticket object into currentOrder's ticket list 
                    currentOrder.TicketList.Add(adultTicket);

                    //Decrements the number of seats remaining for the chosen movie screening by 1 once a ticket has been ordered
                    retrievedMovieScreening.SeatsRemaining--;

                    Console.WriteLine();
                }
            }

            //Catches any error if the user enters an invalid option which is not supported by the program
            catch (Exception exception)
            {
                //Displays the error message which caused the program to break
                Console.WriteLine(exception.Message);

                //Prompts the user to enter a valid option for program to run smoothly
                Console.WriteLine("Please enter a valid option.\n");

                //Automatically returns back to the start of the TicketChecker function for the user 
                //to input the ticket types for each ticket in currentOrder again
                TicketChecker(retrievedMovieScreening, currentOrder);
            }
        }

        //Calculates the total price of the tickets in the current order and displays the price for each ticket as well as the total amount to be paid
        //Updates the order status to be "PAID" when the user confirms their order and makes the payment using the key [Y]
        static void CalculateTicketPrice(List<Order> orderList, Order currentOrder)
        {
            //Empty list ticketPriceList storing data values with type double 
            //Price of each ticket in the ticketList inside the currentOrder object will be added in this list, depending on the ticket type of the ticket
            List<double> ticketPriceList = new List<double>();
            
            //Loops through each ticket in the ticketList storing all the tickets in a single order object (currentOrder)
            foreach (Ticket ticket in currentOrder.TicketList)
            {
                //Calculates the price of each ticket depending on the ticket type selected in the TicketChecker function and stores it in variable with type double tempCalculatePrice
                double tempCalculatePrice = ticket.CalculatePrice();
                
                //Adds the ticket price stored in tempCalculatePrice to the ticketPriceList
                ticketPriceList.Add(tempCalculatePrice);
            }

            //Variable to append the value of each ticket and get the total amount the user is required to pay
            double totalCost = 0;
            
            //Loops through each price in the ticketPriceList 
            foreach (double ticketPrice in ticketPriceList)
            {
                //Adds each ticket's price to totalCost 
                //Final value of totalCost will be the total amount the user needs to pay
                totalCost += ticketPrice;
            }

            //Header for displaying all movies in the list movieList
            Console.WriteLine("=== Ticket Price Breakdown ===");
            
            //Loops through each price in ticketPrice list 
            for (int t = 0; t < ticketPriceList.Count; t++)
            {
                //Displays the ticket number and its corresponding price
                Console.WriteLine($"Ticket No [{t + 1}] = ${ticketPriceList[t]}");
            }

            //Displays sum of all the ticket prices for user to know the total sum to be paid
            Console.WriteLine("==============================");
            Console.WriteLine($"Total Payable = ${totalCost}");
            Console.WriteLine("==============================\n");

            //Message to inform user that their order has been confirmed
            //Prompts user to enter option [Y] to make payment or [N] if they decide to cancel before making any payment
            Console.Write("Your order has been confirmed. Enter [Y] to make payment or [N] to cancel order: ");

            //Code to run continuously until it reaches 'break' and exits the while-loop
            while (true)
            {
                //Reads the user's input and converts it to all uppercase before storing it in variable orderConfirmation
                string orderConfirmation = Console.ReadLine().ToUpper();

                //Code to run if the user enters option [Y] => user makes payment for the order
                if (orderConfirmation == "Y")
                {
                    //Assigns the total amount payable to the Amount constructor in Order object currentOrder 
                    currentOrder.Amount = totalCost;
                    
                    //Updates the Status constructor in Order object currentOrder from "Unpaid" to "Paid"
                    currentOrder.Status = "PAID";
                    
                    //Displays a message to show that the user has successfully paid for his order.
                    Console.WriteLine($"Your order status has been updated to {currentOrder.Status}. Thank you for your purchase!\n");

                    //Breaks out of while-loop to return back to main menu and prompt user to enter another option from the main menu
                    break;
                }

                //Code to run if the user enters option [N] => user cancels the order 
                else if (orderConfirmation == "N")
                {
                    //Removes the order being stored in variable currentOrder from the orderList
                    orderList.Remove(currentOrder);
                    
                    //Message to show the user that the order which was being created has been removed from list orderList and cancellation has been successful
                    Console.WriteLine("Order cancelled.\n");

                    //Breaks out of while-loop to return back to main menu and prompt user to enter another option from the main menu
                    break;
                }

                //Code to run if the user enters an invalid option
                else
                {
                    //Prompts user to enter a valid option to either confirm or cancel order of tickets
                    Console.Write("Enter [Y] to confirm the payment or [N] to cancel the order: ");
                }
            }
        }

        //[6] Cancel order of ticket
        static void CancelTicketOrder(List<Order> orderList, List<Screening> screeningList)
        {
            //Code to run if the orderList contains at least 1 order
            if (orderList.Count > 0)
            {
                //Header for displaying all movies in the list movieList
                Console.WriteLine("============================== List of Ticket Orders ===============================\n");
                Console.WriteLine("{0,-10}{1,-22}{2,-14}{3,-26}{4,-15}", "Order No", "Movie Title", "Amount Paid", "Date of Purchase", "Order Status");
                
                //Loops through each order in the list orderList 
                foreach (Order order in orderList)
                {
                    //Displays the information of each order in the orderList based on the properties of the order specified to be displayed
                    Console.WriteLine("{0,-10}{1,-22}{2,-14}{3,-26}{4,-15}", order.OrderNo, order.TicketList[0].Screening.Movie.Title, $"${order.Amount:F}", order.OrderDateTime, order.Status);
                }

                try
                {
                    //Prompts the user to enter an order number to be cancelled as displayed in the table or option [0] to return back to the main menu
                    Console.Write("\nEnter order number to be cancelled [Enter 0 to exit]: ");
                    
                    //Converts the user's input of the order they wish to cancel into type integer and stores it in variable orderToCancel
                    int orderToCancel = Convert.ToInt32(Console.ReadLine());

                    //Code to run if the user enters the option [0]
                    if (orderToCancel == 0)
                    {
                        //Displays an "Exiting cancellation mode to main menu..." message to show user that it is going back into main menu
                        Console.WriteLine("Exiting cancellation mode to main menu...\n");

                        //Return prevents code outside this if-else statement from running and the function to stop running
                        //and go back to main menu to prompt user to choose an option from the main menu again
                        return;
                    }

                    //Defines the variable orderToRemove with data type Order as null 
                    Order orderToRemove = null;


                    while (true)
                    {
                        //Loops through each order in the list orderList 
                        foreach (Order order in orderList)
                        {
                            //Code to run if the order number entered by the user which was stored in orderToCancel is equal to an order's order number in the orderList
                            if (orderToCancel == order.OrderNo)
                            {
                                //Loops through each screening in the list screeningList
                                foreach (Screening screening in screeningList)
                                {
                                    //Accesses the screening number of the order object to be cancelled
                                    //Subtracts 1001 from the screening number to obtain the index of the screening number of the order and stores it in variable tempScreeningNo 
                                    int tempScreeningIndex = order.TicketList[0].Screening.ScreeningNo - 1001;

                                    //Stores the screening object from screeningList based on the index stored in tempScreeningIndex in variable with data type Screening tempScreening
                                    Screening tempScreening = screeningList[tempScreeningIndex];
                                    
                                    //Code to run if the date time of the order is after the screening's opening date time and the screening's movie title matches the order's movie title => cannot be cancelled
                                    if (order.OrderDateTime >= tempScreening.ScreeningDateTime && order.TicketList[0].Screening.Movie.Title == tempScreening.Movie.Title)
                                    {
                                        //Displays message to tell the user that the order cannot be cancelled as the movie has already been screened
                                        Console.WriteLine("Unable to cancel this order because the movie has been screened.\n");

                                        //Return prevents code outside this if-else statement from running and the function to stop running
                                        //and go back to main menu to prompt user to choose an option from the main menu again
                                        return;
                                    }

                                    //Code to run if the date time of the order is before the screening's opening date time and the screening's movie title matches the order's movie title => can be cancelled
                                    else if (order.OrderDateTime <= tempScreening.ScreeningDateTime && order.TicketList[0].Screening.Movie.Title == tempScreening.Movie.Title)
                                    {
                                        //Counts the number of tickets in the Order object's ticketList to determine the number of tickets to be cancelled and stores it in ticketsToCancel
                                        int ticketsToCancel = order.TicketList.Count;

                                        //Appends the number of tickets to be cancelled and adds it back to tempScreening Screening object since the seats can be released back to the public to be booked
                                        tempScreening.SeatsRemaining += ticketsToCancel;

                                        //Modifies the status of the Order object from "Paid" to "Cancelled"
                                        order.Status = "CANCELLED";

                                        //Displays a message to show the user that the order's status has been updated from "Paid" to "Cancelled"
                                        Console.WriteLine($"Your order status has been updated to {order.Status}.");

                                        //Assigns the Order object to the variable orderToRemove as the order object to be removed
                                        orderToRemove = order;

                                        //Displays the amount that has been refunded back to the users upon the cancellation of the order
                                        Console.WriteLine($"${order.Amount:0.00} has been refunded.");

                                        //Displays a final message to show the user that the order object has been removed successfully from the orderList
                                        Console.WriteLine("Your order has been cancelled successfully.\n");

                                        //Breaks out of the foreach loop to go back into the CancelTicketOrder function
                                        break;
                                    }

                                    //Code to run if the order's movie title does not match the screening's movie title
                                    else if (order.TicketList[0].Screening.Movie.Title != screening.Movie.Title)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }

                        //Code to run if the order to be removed is null => invalid option was entered and order does not exist in list orderList 
                        if (orderToRemove == null)
                        {
                            //Displays message to user that an invalid selection was made
                            Console.WriteLine("Invalid selection.\n");

                            //Automatically returns back to the start of the CancelTicketOrder function for the user 
                            //to attempt cancelling a ticket order again
                            CancelTicketOrder(orderList, screeningList);

                            //Return prevents code outside this if-else statement from running and the function to stop running
                            //and go back to main menu to prompt user to choose an option from the main menu again
                            return;
                        }

                        //Code to run if the order to be removed contains a valid Order object => valid option was entered and order exists in list orderList
                        else
                        {
                            //Removes the order being stored in variable orderToRemove from the orderList
                            orderList.Remove(orderToRemove);

                            //Breaks out of the if-else block and executes the final else block
                            break;
                        }
                    }
                }

                //Catches any error if the user enters an invalid option which is not supported by the program
                catch (Exception exception)
                {
                    //Displays the error message which caused the program to break
                    Console.WriteLine(exception.Message);

                    //Prompts the user to enter a valid option for program to run smoothly
                    Console.WriteLine("Please enter a valid option.\n");

                    //Automatically returns back to the start of the CancelTicketOrder function for the user 
                    //to try cancelling a valid order again
                    CancelTicketOrder(orderList, screeningList);
                }
            }

            //Code to run if there are no order objects stored in list orderList
            else
            {
                //Displays message notifying user that there are ticket orders made so far => no orders to display
                Console.WriteLine("There are currently no ticket orders.\n");
            }
        }

        //[7] Recommend a movie based on the sale of tickets sold
        static void MovieRecommendation(List<Screening> screeningList, List<Movie> movieList, List<Order> orderList)
        {
            //Code to run if there have been no orders made so far
            if (orderList.Count == 0)
            {
                //Display message to remind user that there is no order history => unable to recommend movie to user
                Console.WriteLine("There is no order history for this movie.");

                //Return prevents code outside this if-else statement from running and the function to stop running
                //and go back to main menu to prompt user to choose an option from the main menu again
                return;
            }

            //Header for displaying recommended movie for the user
            Console.WriteLine("=== Recommended movies ===");

            //Loops through each movie in list movieList
            foreach (Movie movie in movieList)
            {
                //Variable to store the total number of seat remaining for all the screening sessions of each movie with initial value 0
                int totalSeatsRemaining = 0;

                //Variable to store the total number of seat available for all the screening sessions of each movie with initial value 0
                int totalAvaliableSeats = 0;

                //Loops through each screening in list screeningList
                foreach (Screening screening in screeningList)
                {
                    //Code to run when the screening's movie title matches a movie's title
                    if (screening.Movie.Title == movie.Title)
                    {
                        //Appends the number of seats remaining for each screening session to variable totalSeatsRemaining
                        totalSeatsRemaining += screening.SeatsRemaining;

                        //Appends the total number of seats available in each cinema hall for each screening session to variable totalAvailableSeats
                        totalAvaliableSeats += screening.Cinema.Capacity;
                    }
                }

                //Code to run if totalAvialableSeats equals to 0 => no screenings for the movie chosen
                if (totalAvaliableSeats == 0)
                {
                    //Displays message for user to know that there are no screening sessions for movie specified
                    Console.WriteLine($"There are no screening sessions available for {movie.Title}");
                }

                //Code to run if totalAvailableSeats does not equate to 0 => screening sessions available for movie specified
                else
                {
                    //Converts value of totalSeatsRemaining to data type double 
                    //Divides the total number of seats remaining by the total number of seats available originally to obtain the percentage of tickets sold for each movie
                    double ticketSalesPercentage = Convert.ToDouble(totalSeatsRemaining) / totalAvaliableSeats * 100.0;

                    //Code to run when the value stored in variable ticketSalesPercentage does not equate to 100 => tickets have been sold for the movie specified
                    if (ticketSalesPercentage != 100)
                    {
                        //Displays the percentage of tickets available for each movie
                        Console.WriteLine($"{ticketSalesPercentage:0.00}% of tickets are available for {movie.Title}. {totalSeatsRemaining} / {totalAvaliableSeats} tickets remaining.");

                    }
                }
            }
            Console.Write("\n");
        }

        //[8] Display the number of available seats in each screening session in descending order
        static void AvailableSeatsInDescOrder(List<Screening> screeningList)
        {
            //Header for user to know that they are viewing the availability of seats in each screening session in descending order
            Console.WriteLine("\nYou are currently viewing the availability of seats of the screening sessions in descending order.");
            
            //Sorts the screeningList based on the factor SeatsRemaining for each screening in the screeningList
            screeningList.Sort();

            //Reverses the order of the screenings in the screeningList such that the seats remaining is displayed in
            //descending order => screening session with a larger availability are displayed first
            screeningList.Reverse();

            //Header for displaying data related to movie screening session
            Console.WriteLine("{0,-23}{1,-16}{2,-18}{3,-18}{4,-18}{5,-20}", "Movie Title", "Cinema Hall", "Cinema Hall No", "Seats Allocated", "Seats Remaining", "Date and Time");
            
            //Loops through every screening in list screeningList
            foreach (Screening screening in screeningList)
            {
                //Displays the information of each screening in screeningList specified to be displayed => included the screening date time to show no duplicated in the screening displayed
                Console.WriteLine("{0,-23}{1,-16}{2,-18}{3,-18}{4,-18}{5,-20}", screening.Movie.Title, screening.Cinema.Name, screening.Cinema.HallNo, screening.Cinema.Capacity, screening.SeatsRemaining, screening.ScreeningDateTime);
            }
            Console.WriteLine();
        }

        //[9] Top 3 movies based on tickets sold
        static void Top3Movie(List<Screening> screeningList, List<Movie> movieList, List<Order> orderList)
        {
            //Sorts the movieList based on the factor number of tickets sold for each movie in the movieList
            movieList.Sort();

            //Counter to ensure that only the top 3 movies are displayed
            int count = 1;

            //Code to run if there are no orders in list orderList
            if (orderList.Count == 0)
            {
                //Display message to show users that there is no order history => unable to  display the top 3 movies.
                Console.WriteLine("There is no order history. Top 3 movies can only be displayed when there is at least 1 order history.\n");

                //Return prevents code outside this if-else statement from running and the function to stop running
                //and go back to main menu to prompt user to choose an option from the main menu again
                return;
            }

            //Header for displaying the top 3 movies in list movieList
            Console.WriteLine("=== Top 3 movies based on ticket sold ===");

            //Loops through every movie object in list movieList
            foreach (Movie movie in movieList)
            {
                //Ticket counter to count the number of tickets sold for each screening 
                int totalTicketSold = 0;

                //Loops through every screening in the screeningList stored in movie class
                foreach(Screening screening in movie.ScreeningList)
                {
                    //Stores a movie screening session's cinema capacity in a variable with data type integer capacity
                    int capacity = screening.Cinema.Capacity;

                    //Stores a movie screening session's seats remaining in a variable with data type integer seatsRemaining
                    int seatsRemaining = screening.SeatsRemaining;

                    //Derives the number of tickets sold for each screening session by subtracting the value stored in seatsRemaining from capacity
                    int ticketSold = capacity - seatsRemaining;

                    //Appends the value ticketSold to totalTicketSold which gives the sum of all the tickets sold for each movie
                    totalTicketSold += ticketSold;
                }

                //Code to run while the counter count is less than or equal to 3 => to display top 3 movies selling the most tickets
                if (count <= 3)
                {
                    //Displays message according to data specified to be displayed to show the total number of tickets sold for each of the top 3 movies
                    Console.WriteLine($"[{count}] {totalTicketSold} tickets were sold for {movie.Title}.");

                    //Increments counter count by 1 after displaying the message above each time
                    count++;
                }
            }
            Console.Write("\n");
        }
    }
}
