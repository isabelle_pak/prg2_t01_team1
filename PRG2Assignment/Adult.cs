﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    internal class Adult:Ticket
    {
        public bool PopcornOffer { get; set; }
        public Adult() { }
        public Adult(Screening screening, bool popcornOffer):base(screening)
        {
            PopcornOffer = popcornOffer;
        }
        public override double CalculatePrice()
        {
            List<DayOfWeek> weekend = new List<DayOfWeek>() { DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };

            double ticketPrice = 0;

            if (Screening.ScreeningType == "3D")
            {
                if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                {
                    ticketPrice = 14;
                }
                else
                {
                    ticketPrice = 11;
                }
            }
            else
            {
                if (weekend.Contains(Screening.ScreeningDateTime.DayOfWeek))
                {
                    ticketPrice = 12.5;
                }
                else
                {
                    ticketPrice = 8.5;
                }
            }

            if (PopcornOffer == true)
            {
                ticketPrice += 3;
                return ticketPrice;
            }
            else
            {
                return ticketPrice;
            }
        }
        public override string ToString()
        {
            return base.ToString() + "\tPopcorn Offer: " + PopcornOffer;
        }
    }
}
