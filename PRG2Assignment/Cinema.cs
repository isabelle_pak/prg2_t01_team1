﻿//============================================================
// Student Number : S10222456, S10227333
// Student Name : Isabelle Pak Yi Shan, Cheah Seng Jun
// Module Group : T01
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Cinema //Done by: Seng Jun
    {
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }
        public Cinema() { }
        public Cinema(string name, int hallNo, int capacity)
        {
            Name = name;
            HallNo = hallNo;
            Capacity = capacity;
        }
        public override string ToString()
        {
            return "Name: " + Name + "\tHall No: " + HallNo + "\tCapacity: " + Capacity;
        }
    }
}
